import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  KeyboardAvoidingView,
  FlatList,
  TouchableOpacity,
  Alert,
} from 'react-native';

import Color from '../const/Colors';
import Constants from '../const/Constants';
import String from '../const/String';

import MessageFieldView from '../components/MessageFieldView';
import DismissKeyboard from '../components/DismissKeyboard';
import MessageItem from '../components/MessageItem';

import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';

const ChatScreen = ({route, navigation}) => {
  const [messageList, setMessageList] = useState([]);
  const [message, setMessage] = useState('');
  const [isJoined, setIsJoined] = useState(false);

  const {item} = route.params;
  const userID = auth().currentUser.uid;
  const userEmail = auth().currentUser.email;

  useEffect(() => {
    getUserJoinedAlreadyOrNot();
    getMessages();
  }, []);

  function getUserJoinedAlreadyOrNot() {
    firestore()
      .collection('members')
      .doc(item.groupId)
      .collection('member')
      .where('userID', '==', userID)
      .get()
      .then(function (querySnapshot) {
        console.log('test ', querySnapshot.size);
        if (querySnapshot.size > 0) {
          querySnapshot.forEach(function (doc) {
            if (doc.data() != null) {
              setIsJoined(true);
            } else {
              showAlertToJoinGroup();
              setIsJoined(false);
            }
          });
        } else {
          showAlertToJoinGroup();
          setIsJoined(false);
        }
      })
      .catch(function (error) {
        console.log('Error getting documents: ', error);
      });
  }

  function showAlertToJoinGroup() {
    Alert.alert(
      String.JoinChat,
      String.JoinChatConfirmMessage,
      [
        {
          text: 'Yes',
          onPress: () => {
            joinGroup();
          },
        },
        {
          text: 'No',
          onPress: () => {},
        },
      ],
      {cancelable: false},
    );
  }

  function joinGroup() {
    const groupMemberRef = firestore()
      .collection('members')
      .doc(item.groupId)
      .collection('member')
      .doc();

    groupMemberRef
      .set({
        userID: userID,
      })
      .then(function (docRef) {
        setIsJoined(true);
        setMessage('');
        Alert.alert(String.joinMsg);
      })
      .catch(function (error) {
        setIsJoined(false);
        Alert.alert(String.JoinGroupError);
      });
  }

  function getMessages() {
    var messages = [];

    firestore()
      .collection('messages')
      .doc(item.groupId)
      .collection('message')
      .onSnapshot(function (snapshot) {
        snapshot.docChanges().forEach(function (change) {
          if (change.type == 'added') {
            console.log('New MSG: ', change.doc.data());
            messages.push(change.doc.data());
          }
          if (change.type === 'modified') {
            console.log('Modified MSG: ', change.doc.data());
          }
          if (change.type === 'removed') {
            console.log('Removed MSG', change.doc.data());
          }

          setMessageList(messages);
        });
      });
  }

  function sendMsgToChat() {
    const msgRef = firestore()
      .collection('messages')
      .doc(item.groupId)
      .collection('message')
      .doc();

    msgRef
      .set({
        messageID: msgRef.id,
        message: message,
        senderId: userID,
        senderEmail: userEmail,
      })
      .then(() => {
        //Alert.alert('Document written with ID: ', msgRef.id);
        setMessage('');
      })
      .catch(function (error) {
        Alert.alert('Error: ' + error);
      });
  }

  return (
    <DismissKeyboard>
      <KeyboardAvoidingView
        style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'center',
        }}
        behavior="padding"
        enabled
        keyboardVerticalOffset={100}>
        <View style={styles.container}>
          <FlatList
            style={styles.flatList}
            data={messageList}
            keyExtractor={(item, index) => 'key' + index}
            renderItem={({item}) => {
              return (
                <TouchableOpacity onPress={() => {}}>
                  <MessageItem item={item} />
                </TouchableOpacity>
              );
            }}></FlatList>
          <View style={styles.messageFieldView}>
            <MessageFieldView
              term={message}
              placeHolder={String.typeYourMessage}
              onTermChange={message => setMessage(message)}
              onSubmit={sendMsgToChat}></MessageFieldView>
          </View>
        </View>
      </KeyboardAvoidingView>
    </DismissKeyboard>
  );
};

export default ChatScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  flatList: {
    marginBottom: 10,
    flex: 0.9,
  },
  messageFieldView: {
    flex: 0.14,
  },
});
