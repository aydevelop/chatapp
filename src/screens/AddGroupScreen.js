import React, {useState} from 'react';
import {Alert, StyleSheet, Text, View} from 'react-native';
import CustomTextField from '../components/CustomTextField';
import Button from '../components/Button';
import Colors from '../const/Colors';
import Strings from '../const/String';
import Utility from '../utils/Utility';

import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';

const AddGroupScreen = ({navigation}) => {
  const [groupName, setGroupName] = useState('');
  const [fieldError, setFieldError] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  const validateField = () => {
    const isValidField = Utility.isValidField(groupName);

    if (isValidField) {
      setFieldError('');
    } else {
      setFieldError(Strings.GroupNameEmpty);
    }

    return isValidField;
  };

  const performCreateGroup = () => {
    const isValidField = validateField();
    if (!isValidField) {
      return;
    }

    setIsLoading(true);
    const groupsRef = firestore().collection('groups').doc();
    const gId = groupsRef.id;
    const userID = auth().currentUser.uid;

    groupsRef
      .set({
        groupId: gId,
        groupName: groupName,
        userID: userID,
      })
      .then(() => {
        //Alert.alert('Document written with ID: ', gId);
        addMembersOfChatTtoFirebase(gId, userID);
      })
      .catch(function (error) {
        Alert.alert('Error: ' + error);
      })
      .finally(function () {
        setIsLoading(false);
      });
  };

  function addMembersOfChatTtoFirebase(groupId, userID) {
    const membersRef = firestore()
      .collection('members')
      .doc(groupId)
      .collection('member')
      .doc();

    membersRef
      .set({
        userID: userID,
      })
      .then(function () {
        navigation.goBack();
      })
      .catch(function (error) {
        setIsLoading(false);
        Alert.alert('Error adding document: ' + error);
      });
  }

  return (
    <View style={styles.container}>
      <CustomTextField
        term={groupName}
        error={fieldError}
        placeHolder={Strings.EnterYourGroupName}
        onTermChange={newGroupName => setGroupName(newGroupName)}
        onValidateTextField={validateField}
      />

      <Button
        title={Strings.CreateGroup}
        isLoading={isLoading}
        onPress={performCreateGroup}
      />
    </View>
  );
};

export default AddGroupScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
