import React, {useLayoutEffect, useState, useEffect} from 'react';
import {StyleSheet, Text, View, FlatList, TouchableOpacity} from 'react-native';
import ButtonWithBackground from '../components/ButtonWithBackground';
import Images from '../const/Images';
import GroupItem from '../components/GroupsItems';
import firestore from '@react-native-firebase/firestore';

const GroupsScreen = ({navigation}) => {
  const [groups, setGroups] = useState([1, 2, 3]);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <ButtonWithBackground
          onPress={() => {
            navigation.navigate('Add Group Screen');
          }}
          image={Images.add}
        />
      ),
      // headerLeft: () => {
      //   <Text>Some text2....</Text>;
      // },
    });
  });

  useEffect(() => {
    //getChats();

    // const subscriber = firestore()
    //   .collection('groups')
    //   .onSnapshot(documentSnapshot => {
    //     console.log('User data: ', documentSnapshot);
    //   });

    // firestore()
    //   .collection('groups')
    //   .get()
    //   .then(querySnapshot => {
    //     console.log('Total users: ', querySnapshot.size);

    //     querySnapshot.forEach(documentSnapshot => {
    //       console.log(
    //         'User ID: ',
    //         documentSnapshot.id,
    //         documentSnapshot.data(),
    //       );
    //     });
    //   });

    getChats();

    // Stop listening for updates when no longer required
  }, []);

  function getChats() {
    var groupArray = [];
    firestore()
      .collection('groups')
      .onSnapshot(function (snapshot) {
        snapshot.docChanges().forEach(function (change) {
          if (change.type == 'added') {
            console.log('New Group: ', change.doc.data());
            groupArray.push(change.doc.data());
          }
          if (change.type === 'modified') {
            console.log('Modified Group: ', change.doc.data());
          }
          if (change.type === 'removed') {
            console.log('Removed Group', change.doc.data());
          }

          setGroups(groupArray);
        });
      });
  }

  return (
    <View style={styles.container}>
      <FlatList
        data={groups}
        keyExtractor={(item, index) => 'key' + index}
        renderItem={({item}) => {
          return (
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('Chat Screen', {
                  item,
                });
              }}>
              <GroupItem item={item}></GroupItem>
            </TouchableOpacity>
          );
        }}></FlatList>
    </View>
  );
};

export default GroupsScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ebebeb',
  },
  text: {
    color: '#101010',
    fontSize: 24,
    fontWeight: 'bold',
  },
});
