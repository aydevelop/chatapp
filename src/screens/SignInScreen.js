import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Alert,
  SafeAreaView,
  Image,
  KeyboardAvoidingView,
} from 'react-native';
import Button from '../components/Button';
import EmailTextField from '../components/EmailTextField';
import PasswordTextField from '../components/PasswordTextField';
import Color from '../const/Colors';
import Strings from '../const/String';
import Constants from '../const/Constants';
import Images from '../const/Images';
import DismissKeyboard from '../components/DismissKeyboard';
import Utility from '../utils/Utility';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';

import {LogBox} from 'react-native';
//LogBox.ignoreWarnings(['Setting a timer']);
LogBox.ignoreAllLogs();

const SignInScreen = ({navigation}) => {
  const [email, setEmail] = useState('root@gmail.com');
  const [password, setPassword] = useState('111111');
  const [emailError, setEmailError] = useState('');
  const [passwordError, setPasswordError] = useState('');
  const [isLoading, setIsLoading] = useState('');

  const validateEmailAddress = () => {
    const isValidEmail = Utility.isEmailValid(email);

    isValidEmail
      ? setEmailError('')
      : setEmailError(Strings.InvalidEmailAddress);
    return isValidEmail;
  };

  const validatePasswordField = () => {
    const isValidField = Utility.isValidField(password);
    isValidField
      ? setPasswordError('')
      : setPasswordError(Strings.PasswordFieldEmpty);
    return isValidField;
  };

  const performAuth = async () => {
    const isValidEmail = validateEmailAddress();
    const isValidPassword = validatePasswordField();

    if (isValidEmail && isValidPassword) {
      setEmailError('');
      setPasswordError('');
      registration(email, password);
    }
  };

  const registration = (email, password) => {
    try {
      setIsLoading(true);
      //alert('registration');

      // auth()
      //   .signInWithEmailAndPassword(email, password)
      //   .then(() => {
      //     alert('User account created & signed in!');
      //   })
      //   .catch(error => {
      //     alert('error: ' + error.code);
      //   });

      //firebaseApp
      auth()
        .signInWithEmailAndPassword(email, password)
        .then(user => {
          setIsLoading(false);
          //Alert.alert('Logged In');
          navigation.reset({
            index: 0,
            routes: [{name: 'Groups Screen'}],
          });
        })
        .catch(error => {
          //console.log('msg2::: ' + error);
          //return Alert.alert('msg::: ' + error);
          //firebaseApp
          auth()
            .createUserWithEmailAndPassword(email, password)
            .then(user => {
              setIsLoading(false);
              Alert.alert('Create A New user');
            })
            .catch(error => {
              setIsLoading(false);
              console.log('error');
              Alert.alert(error.message);
            });
        });
    } catch (error) {
      setIsLoading(false);
      Alert.alert(error.message);
    }
  };

  return (
    <DismissKeyboard>
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
        <View style={styles.container}>
          <SafeAreaView>
            <Image style={styles.logo} source={Images.logo}></Image>
            <EmailTextField
              term={email}
              error={emailError}
              placeHolder={Strings.EmailPlaceHolder}
              onTermChange={newEmail => {
                setEmail(newEmail);
              }}
              onValidateEmailAddress={validateEmailAddress}
            />
            <PasswordTextField
              term={password}
              error={passwordError}
              placeHolder={Strings.PasswordPlaceHolder}
              onTermChange={newPassword => {
                setPassword(newPassword);
              }}
              onValidatePasswordField={validatePasswordField}
            />
            <Button
              title={Strings.Join}
              onPress={performAuth}
              isLoading={isLoading}
            />
          </SafeAreaView>
        </View>
      </KeyboardAvoidingView>
    </DismissKeyboard>
  );
};

export default SignInScreen;

const styles = StyleSheet.create({
  logo: {
    alignSelf: 'center',
    margin: 0.04 * Constants.screenHeight,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Color.theme,
  },
});
