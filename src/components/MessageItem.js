import React from 'react';
import {TextInput, Text, StyleSheet, View} from 'react-native';
import Color from '../const/Colors';
import Constants from '../const/Constants';
import Images from '../const/Images';

import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';

const MessageItem = ({item}) => {
  const userID = auth().currentUser.uid;

  function messageView() {
    if (userID == item.senderID) {
      return (
        <View style={styles.otherMsgContainerView}>
          <Text style={[styles.senderName, {textAlign: 'right'}]}>
            {item.senderEmail}
          </Text>
          <Text style={[styles.message, {textAlign: 'right'}]}>
            {item.message}
          </Text>
        </View>
      );
    } else {
      return (
        <View style={styles.otherMsgContainerView}>
          <Text style={styles.senderName}>{item.senderEmail}</Text>
          <Text style={[styles.message]}>{item.message}</Text>
        </View>
      );
    }
  }

  return messageView();
};

export default MessageItem;

const styles = StyleSheet.create({
  otherMsgContainerView: {
    width: Constants.screenWidth - 50,
    backgroundColor: Color.theme,
    borderRadius: 5,
    marginLeft: 25,
    marginTop: 10,
    marginBottom: 5,
    padding: 10,
  },
  senderName: {
    color: Color.white,
    fontSize: 14,
    fontWeight: 'bold',
  },
  message: {
    color: Color.white,
    fontSize: 14,
  },
});
