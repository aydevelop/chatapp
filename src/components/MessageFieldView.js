import React from 'react';
import {TextInput, Text, StyleSheet, View, Button} from 'react-native';
import Color from '../const/Colors';
import Constants from '../const/Constants';
import ButtonWithBackground from '../components/ButtonWithBackground';
import String from '../const/String';

const MessageFieldView = ({
  term,
  placeHolder,
  onTermChange,
  onValidateTextField,
  error,
  onSubmit,
  isJoined,
}) => {
  return (
    <View style={styles.containerView}>
      <View style={styles.fieldView}>
        <TextInput
          autoCorrect={false}
          style={styles.textField}
          placeholder={placeHolder}
          value={term}
          onChangeText={onTermChange}
          onEndEditing={onValidateTextField}
        />
        <View style={styles.button2}>
          <Button
            title={String.Send}
            color={Color.green}
            onPress={onSubmit}></Button>
        </View>
      </View>
    </View>
  );
};

export default MessageFieldView;

const styles = StyleSheet.create({
  containerView: {
    backgroundColor: Color.white,
    width: Constants.screenWidth,
    flex: 1,
    justifyContent: 'space-between',
  },
  fieldView: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: Color.green,
  },
  textField: {
    fontSize: 14,
    marginRight: 0,
    paddingLeft: 10,
    width: '75%',
    borderColor: Color.gray,
    borderWidth: 1,
    justifyContent: 'center',
    backgroundColor: Color.smoke,
  },
  button2: {
    justifyContent: 'center',
    flex: 1,
  },
});
